﻿using System;
using System.IO;
using System.Threading.Tasks;

public class FileArgs : EventArgs
{
    public string FileName { get; }
    public bool Cancel { get; set; }

    public FileArgs(string fileName)
    {
        FileName = fileName;
        Cancel = false;
    }
}

public class FileSearcher
{
    public event EventHandler<FileArgs> FileFound;

    // Asynchronously searches the directory for files.
    public async Task SearchAsync(string directory)
    {
        if (!Directory.Exists(directory))
        {
            Console.WriteLine("Directory does not exist.");
            return;
        }

        try
        {
            var files = Directory.GetFiles(directory);
            foreach (var file in files)
            {
                await ProcessFileAsync(file);
            }
        }
        catch (UnauthorizedAccessException ex)
        {
            Console.WriteLine($"Access denied: {ex.Message}");
        }
        catch (PathTooLongException ex)
        {
            Console.WriteLine($"Path too long: {ex.Message}");
        }
        catch (IOException ex)
        {
            Console.WriteLine($"IO error: {ex.Message}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Unexpected error: {ex.Message}");
        }
    }

    // Processes each file and triggers the FileFound event.
    protected async Task ProcessFileAsync(string file)
    {
        var args = new FileArgs(file);
        OnFileFound(args);

        // Optionally introduce a delay or other asynchronous operation if needed.
        await Task.Delay(10); // Simulate some work

        if (args.Cancel)
        {
            // Additional logic for cancellation can be added here.
            // For example:
            // Log cancellation
            Console.WriteLine($"Search cancelled after finding file: {file}");

            // Perform any necessary cleanup
            CleanUp();

            // Set a flag or take other actions based on the cancellation
            // For example, you might want to avoid further processing or reset some state
            HandleCancellation();
        }
    }

    // Invokes the FileFound event.
    protected virtual void OnFileFound(FileArgs e)
    {
        FileFound?.Invoke(this, e);
    }

    private void CleanUp()
    {
        // Code to clean up resources, if any
        // For example, releasing file handles, closing streams, etc.
        Console.WriteLine("Performing cleanup after cancellation.");
    }

    private void HandleCancellation()
    {
        // Code to handle the aftermath of cancellation
        // For example, setting flags, updating UI, etc.
        Console.WriteLine("Handling the cancellation scenario.");
    }
}
