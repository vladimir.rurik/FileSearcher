﻿using System;
using System.Collections.Generic;

class Program
{
    static async Task Main()
    {
        // Utilize GetMax to obtain the maximum value.
        List<string> words = new List<string> { "apple", "orange", "banana", "cucumber" };
        var longestWord = words.GetMax(word => word.Length);
        Console.WriteLine($"The longest word is: {longestWord}");

       // Utilize FileSearcher to find files in a directory.
        var searcher = new FileSearcher();
        searcher.FileFound += (sender, e) =>
        {
            Console.WriteLine($"Found file: {e.FileName}");
            e.Cancel = true; // Terminate the search once a file is found.
        };

        await searcher.SearchAsync("./"); // Replace with your directory path
    }
}
