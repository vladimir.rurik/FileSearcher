# FileSearcher and GetMax Extension Method

This project demonstrates two core functionalities:
1. A generic extension method `GetMax`, which finds the maximum element in a collection based on a specified conversion to a numerical value.
2. A `FileSearcher` class that traverses a directory of files and triggers an event upon finding each file.

## Features

- **GetMax Extension Method**: A generic method to find the maximum element in any collection based on a provided numeric conversion.
- **FileSearcher Class**: Traverses file directories and emits events for each file discovered.
- **Event Handling**: Includes .NET-standard event handling and custom event arguments.
- **Search Cancellation**: Ability to cancel the ongoing search from an event handler.

## Using the GetMax Extension Method

The `GetMax` method is an extension to `IEnumerable<T>` and can be used as follows:

```csharp
List<string> words = new List<string> { "apple", "orange", "banana", "cucumber" };
var longestWord = words.GetMax(word => word.Length);
Console.WriteLine($"The longest word is: {longestWord}");
```

This example finds the longest word in a list of strings.

## Using the FileSearcher Class

The `FileSearcher` class is used to search through a directory of files:

```csharp
var searcher = new FileSearcher();
searcher.FileFound += (sender, e) =>
{
	Console.WriteLine($"Found file: {e.FileName}");
	// Optional: Cancel the search after a specific condition is met.
	// Terminate the search once the initial file is located.
	e.Cancel = true;
};

searcher.Search("path_to_directory"); // Replace with your directory path
```

When a file is found, the `FileFound` event is triggered. The event can be handled to perform actions for each found file. The search can be canceled at any point from within the event handler.

## Setup and Requirements

- .NET SDK (Version 7 or later recommended).
- Visual Studio or another C# development environment.

## Contributing

Contributions to this project are welcome. Feel free to fork the repository, make changes, and submit a pull request.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
